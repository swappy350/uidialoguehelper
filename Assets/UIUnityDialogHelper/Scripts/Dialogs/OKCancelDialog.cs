﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UnityUIDialogHelper
{
    public class OKCancelDialog : MonoBehaviour
    {
        public TextMeshProUGUI TitleText;
        public TextMeshProUGUI MessageText;
        public Button ButtonOK;
        public Button ButtonCancel;
    }
}