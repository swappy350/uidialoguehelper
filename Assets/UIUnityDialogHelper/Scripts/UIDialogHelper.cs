﻿using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

namespace UnityUIDialogHelper
{
    public class UIDialogHelper : MonoBehaviour
    {
        public GameObject SimpleDialogPrefab, OKCancelDialogPrefab;
        private RectTransform RTRootCanvas;
        public static UIDialogHelper Instance;

        private void Awake() 
        {
            if (Instance != null)
            {
                if (Instance != this)
                {
                    Destroy(Instance);
                }
            }
            else
            {
                Instance = this;
            }
        }

        private void Start()
        {
            RTRootCanvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<RectTransform>();
        }

        /// <summary>
        /// Create simple dialog with only ok button which will dismiss this dialog
        /// </summary>
        /// <param name="Message">Message to be shown in dialog box</param>
        public void CreateSimpleDialog(string Message)
        {
            if (RTRootCanvas != null)
            {
                GameObject simpleDialog = Instantiate(SimpleDialogPrefab, RTRootCanvas);
                simpleDialog.GetComponentInChildren<TextMeshProUGUI>().text = Message;
                simpleDialog.GetComponentInChildren<Button>().onClick.AddListener(() => { Destroy(simpleDialog); });
                simpleDialog.SetActive(true);
            }
            else
            {
                Debug.Log("Root Canvas is null, please assign MainCanvas tag to your Main Canvas in this scene");
            }
        }

        /// <summary>
        /// Create Dialog box with ok and cancel button which will return you true or false
        /// </summary>
        /// <param name="Title">Dialog box title</param>
        /// <param name="Message">Dialog box message</param>
        /// <param name="ActionOK">Dialog box OK</param>
        /// <param name="ActionCancel">Dialog box Cancel</param>
        public GameObject CreateOKCancelDialog(string Title, string Message, Action ActionOK, Action ActionCancel)
        {
            GameObject simpleDialog = null;
            if (RTRootCanvas != null)
            {
                simpleDialog = Instantiate(OKCancelDialogPrefab, RTRootCanvas);
                OKCancelDialog dialog = simpleDialog.GetComponent<OKCancelDialog>();
                dialog.TitleText.text = Title;
                dialog.MessageText.text = Message;
                dialog.ButtonOK.onClick.AddListener(() => { ActionOK.Invoke(); Destroy(simpleDialog); });
                dialog.ButtonCancel.onClick.AddListener(() => { ActionCancel.Invoke(); Destroy(simpleDialog); });
                simpleDialog.SetActive(true);
            }
            else
            {
                Debug.Log("Root Canvas is null, please assign MainCanvas tag to your Main Canvas in this scene");
            }
            return simpleDialog;
        }
    }
}
