﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityUIDialogHelper;

public class DialogTest : MonoBehaviour
{
    public void BtnCreateSimpleDialogClick()
    {
        UIDialogHelper.Instance.CreateSimpleDialog("Simple dialog example");
    }

    public void BtnCreateOKCancelDialogClick()
    {
        UIDialogHelper.Instance.CreateOKCancelDialog("OK Cancel Dialog", "Example of ok cancel dialog", () =>
        {
            UIDialogHelper.Instance.CreateSimpleDialog("OK Clicked");
        }, () =>
        {
            UIDialogHelper.Instance.CreateSimpleDialog("Cancel Click");
        });
    }

}
